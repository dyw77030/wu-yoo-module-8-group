<!DOCTYPE HTML>
<?php session_start();
if(!isset($_SESSION['openid.identifier'])){
	header("Location: login.html");
	exit;
}?>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Recipe Sharer</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <script src="js/modernizr.js"></script>
  </head>
  <body>

<form enctype="multipart/form-data" action = 'submit_process.php' method = 'post'>
  <fieldset>
  
  <legend><h1>Make Your Creation</h1></legend>
  
  <div class="row">
    <div class="small-4 columns">
      <label>Name of Recipe</label>
      <input type = 'text' name = 'title' size = '90' placeholder = 'Write your title here.' required/><br>
    </div>
  </div>
  <br>
  
    <div class = "row">
      <div class="small-6 columns">
        <label>Description</label>
        <input type="text" name = 'description' placeholder="Description goes here" required/>
      </div>
    </div>
    <br>
  <div class="row">
    <div class="small-6 columns">
      <label>Ingredients</label>
      <textarea name = 'ingredients' rows = "10" placeholder="Write necessary ingredients here" required></textarea>
    </div>
  </div>
  <br>
  <div class = "row">
     <div class="small-6 columns">
      <label>Steps</label>
      <textarea name = 'steps' rows = "15" placeholder="Write Steps Here" required></textarea>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="small-5 columns">
      <label>Categories</label>
      <select name = 'category'>
        <option value="appetizer">Appetizer</option>
        <option value="main">Main</option>
        <option value="soup">Soup</option>
        <option value="salad">Salad</option>
        <option value="side">Side</option>
        <option value="dessert">Dessert</option>
      </select>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="small-4 columns">
      <label>Video</label>
      <input type = 'text' name = 'video' size = '90' placeholder = 'Paste in a youtube link!'/><br>
    </div>
  </div>
  <br>
  <div class="row">
  <div class="small-4 columns">
   <input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
   <label for="uploadfile_input">Choose an image to upload:</label> <input name="image" type="file" id="uploadfile_input" />
   </div>
   </div>
  <br>
  <input type = 'hidden' name = 'token' value= "<?php echo $_SESSION['token'];?>" />
  <input type = 'submit' value = "Submit Recipe" class = 'button'/>
  
  
  
</fieldset>
</form>
<form action = 'index.php'>
	<input type = 'submit' value = "Back to Home Page" class = 'button'/>
</form>
    <script src="js/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
