<?php
session_start();
//Checking to see if the user is signed in and has submitted information for all fields.
if(!isset($_SESSION['openid.identifier'])){
	header("Location: login.html");
	exit;
}


//Checking CSRF token
if($_SESSION['token'] !== $_POST['token']){
	die("Request forgery detected");
}


session_start();
require 'database.php';

$title = $_POST['title'];
$description = $_POST['description'];
$ingredients = $_POST['ingredients'];
$steps = $_POST['steps'];
$user = $_SESSION['openid.identifier'];
$category = $_POST['category'];
$id = $_POST['id'];

$st = $mysqli->prepare("SELECT img, video FROM public WHERE id = ?");
if(!$st){
	printf("Query Prep Failed1: %s\n", $mysqli->error);
	exit;
}
	
$st->bind_param('i', $id);
$st->execute();
$st->bind_result($img, $video);
$st->fetch();
$st->close();

//Checking for duplicate title
$stmt2 = $mysqli->prepare("SELECT count(*) FROM public WHERE title = ? && user= ?");
if(!$stmt2){
	printf("Query Prep Failed2: %s\n", $mysqli->error);
	exit;
}
	
$stmt2->bind_param('ss', $title, $user);
$stmt2->execute();
$stmt2->bind_result($cnt);
$stmt2->fetch();
$stmt2->close();

//Insertion
if ($cnt == 0){
	$stmt = $mysqli->prepare("INSERT INTO private_rec (title, ingred, steps, user, video, descrip, category, img, pub_key) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
	if(!$stmt){
		printf("Query Prep Failed3: %s\n", $mysqli->error);
		exit;
	}
	$stmt -> bind_param('sssssssss', $title, $ingredients, $steps, $user, $video, $description, $category, $img, $id);
	$stmt -> execute();
	$stmt -> close();
	header("Location: index.php");
	exit;

}
else{
	header("Location: index.php");
	exit;
}
?>