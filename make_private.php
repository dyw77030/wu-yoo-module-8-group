<?php
session_start();
if(!isset($_POST['id'])){
    header('Location: login.html');
}
$id = $_POST['id'];
if(!isset($_SESSION['openid.identifier'])){
	header("Location: login.html");
	exit;
}
?>

<!DOCTYPE HTML>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Recipe Sharer</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <script src="js/modernizr.js"></script>
  </head>
  <body>
  
<?php
require 'database.php';
$story_data = $mysqli->prepare("select title, ingred, steps, descrip from public where id=?");
if(!$story_data){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
}
	
$story_data->bind_param('i', $id);
$story_data->execute();
$story_data->bind_result($title, $ingred, $steps, $descrip);
while($story_data->fetch()){
	$ingred = str_replace("<br />", "", $ingred);
	$steps = str_replace("<br />", "", $steps);
    printf("
  <form enctype='multipart/form-data' action = 'submit_private.php' method = 'post'>
  <fieldset>
  
  <legend><h1>Make Your Creation</h1></legend>
  
  <div class='row'>
    <div class='small-4 columns'>
      <label>Name of Recipe</label>
      <input type = 'text' name = 'title' size = '90' value ='%s' required/><br>
    </div>
  </div>
  <br>
  
    <div class = 'row'>
      <div class='small-6 columns'>
        <label>Description</label>
        <input type='text' name = 'description' value ='%s' required/>
      </div>
    </div>
    <br>
  <div class='row'>
    <div class='small-6 columns'>
      <label>Ingredients</label>
      <textarea name = 'ingredients' rows = '10' required>%s</textarea>
    </div>
  </div>
  <br>
  <div class = 'row'>
     <div class='small-6 columns'>
      <label>Steps</label>
      <textarea name = 'steps' rows = '15' required>%s</textarea>
    </div>
  </div>
  <br>
  <div class='row'>
    <div class='small-5 columns'>
      <label>Categories</label>
      <select name = 'category'>
        <option value='appetizer'>Appetizer</option>
        <option value='main'>Main</option>
        <option value='soup'>Soup</option>
        <option value='salad'>Salad</option>
        <option value='side'>Side</option>
        <option value='dessert'>Dessert</option>
      </select>
    </div>
  </div>
  <br>
  <input type = 'submit' value = 'Submit Recipe' class = 'btn'/>
  <input type = 'hidden' name = 'id' value='%s' />
  <input type = 'hidden' name = 'token' value='%s' /> 
</fieldset>
",
htmlspecialchars( $title ),
nl2br(htmlspecialchars( $descrip )),
htmlspecialchars($ingred),
htmlspecialchars($steps ),
htmlspecialchars( $id ),
$_SESSION['token']
);
}

?>
</body>
</html>
