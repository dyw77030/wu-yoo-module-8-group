<!DOCTYPE HTML>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Recipe Sharer</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <script src="js/modernizr.js"></script>
  </head>
  <body>
 
  <div class="row">
    <div class="large-12 columns">
      <div class="nav-bar right">
       <ul class="button-group">
         <li><a href="login.html" class="button">Login</a></li>
         <li><a href="private_list.php" class="button">Private Recipes</a></li>
         <li><a href="createRecipe.php" class="button">Create a Recipe</a></li>
         <li><a href="logout.php" class="button">Logout</a></li>
        </ul>
      </div>
      <h1><a href="index.php">Recipe Sharer</a></h1>
      <hr />
    </div>
  </div>


<?php
	session_start();
	if(!isset($_SESSION['openid.identifier'])){
	header("Location: login.html");
	exit;
}
	require 'database.php';
    	$story_data = $mysqli->prepare("select id, title, img, category, descrip, user from public");
	if(!$story_data){
	    printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
	}
	$story_data->execute();
	$story_data->bind_result($id, $title, $img, $category,$descrip, $user);
	while($story_data->fetch()){
        print("<div class='row'>");
	print("<div class='large-9 columns' role='content'>");
	printf("
	      <article>
	      <div class='row'>
	      <div class='large-12 columns' role='content'>
		  <h3><a href='view_public.php?p=%s'>%s</a></h3>
			  <div class='large-6 columns'>
			  <img src='%s' />
			  </div>
	      </div>
	      </div>

	      <div class='row'>
	      <div class='large-6 columns'>
	      <h6>Category: %s</h6>
	      <h6>%s</h6>
	      </div>
	      </div>
	      
	      </article>
	      <form action = 'make_private.php' method = 'post'>
	      <input type = 'hidden' name = 'id' value='%s' />
	      <input type = 'submit' value = 'Make This Yours' class = 'button'/>
	      </form>
		  ",
		  htmlspecialchars( $id ),
		  htmlspecialchars( $title ),
		  htmlspecialchars( $img ),
		  nl2br(htmlspecialchars( $category )),
		  nl2br(htmlspecialchars( $descrip )),
		  htmlspecialchars( $id )
            );
	if(isset($_SESSION['openid.identifier'])){
	  if($_SESSION['openid.identifier'] == $user){
	    printf("
		   <form action = 'edit_recipe.php' method = 'post'>
		   <input type = 'hidden' name = 'id' value='%s' />
		   <input type = 'hidden' name = 'token' value='%s' /> 
		   <input type = 'submit' value = 'Edit Your Recipe' class = 'button'/>
		   </form>", htmlspecialchars( $id ),  $_SESSION['token']);
	  }
	}
	print("<hr/>");
        print("</div></div>");
	}		
	$story_data->close();
?> 
  <footer class="row">
    <div class="large-12 columns">
      <hr />
      <div class="row">
        <div class="large-6 columns">
          <p>© Copyright Jeffrey Yoo and Dennis Wu</p>
        </div>
      </div>
    </div>
  </footer>
    
    
    <script src="js/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
