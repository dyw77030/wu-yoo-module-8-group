<?php
	session_start();

//Checking to see if the user is signed in and has submitted information for all fields.
if(!isset($_SESSION['openid.identifier'])){
	header("Location: login.html");
	exit;
}
?>

<!DOCTYPE html>
<html>
<head><title>Viewing Your Recipe</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <script src="js/modernizr.js"></script>
</head>
<body>
	
	<div class="row">
	    <div class="large-12 columns">
	      <div class="nav-bar right">
	       <ul class="button-group">
         <li><a href="login.html" class="button">Login</a></li>
         <li><a href="private_list.php" class="button">Private Recipes</a></li>
         <li><a href="createRecipe.php" class="button">Create a Recipe</a></li>
         <li><a href="logout.php" class="button">Logout</a></li>
	        </ul>
	      </div>
	      <h1><a href="index.php">Recipe Sharer</a></h1>
	      <hr />
	    </div>
	</div>
	
	<div class="row">
	<div class="large-9 columns" role="content">
	<article>
		<?php
			require 'database.php';
			$id = htmlspecialchars($_GET['p']);
			$user = htmlspecialchars($_SESSION['openid.identifier']);
			
			$story_data = $mysqli->prepare("select title, category, ingred, steps, img, video, descrip from private_rec where user=? and id = ?");
			if(!$story_data){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}
			
			$story_data->bind_param('si',$user, $id);
				
			$story_data->execute();
	 
			$story_data->bind_result($title, $category, $ingred, $steps, $img, $video, $descrip);
			
			while($story_data->fetch()){
				if(isset($_GET['scale'])&&$_GET['scale']!=null){
					$toScale = $_GET['scale'];
					$num_regex = "/(\d*\S{0,1}\d+\s)/";
					preg_match_all($num_regex, $ingred, $nums);
					$scaledNums = array();
					$matchNums = array();
					$origNums = array();
					$lim = 1;
					$c = 'A';
					$chars = array($c."dfs");
					$charmatch = array("/(".$c."dfs".")/");
					foreach($nums[1] as $num){
						array_push($origNums, $num);
						$res = (float)$num*(float)$toScale;
						$resString = (string)$res." "; 
						array_push($matchNums, "/(".$num.")/");
						array_push($scaledNums, $resString);
						$chars[] = ++$c."dfs";
						$charmatch[] = "/(".$c."dfs".")/";
					}
					for($x = 0; $x < sizeof($scaledNums); $x++){
						$ingred = preg_replace($matchNums[$x], $chars[$x], $ingred, $lim);
					}
					for($x = 0; $x < sizeof($scaledNums); $x++){
						$ingred = str_replace($chars[$x], $scaledNums[$x], $ingred);
					}
				}
				
				print("<div class='large-9 columns' role='content'>");
				printf("
				        <h3>%s</h3>
					<div class='row'>
				        <div class='large-6 columns'>
						<img src='%s' />
					</div>
					</div>
				        <div class='row'>
						<div class='large-6 columns'>
						<h4>Category: %s</h4>
						<h4>%s</h4>
						<h4>%s</h4>
						<h4>%s</h4>
						</div>
				        </div>
					
				       ",
					htmlspecialchars( $title ),
					htmlspecialchars( $img ),
					htmlspecialchars( $category ),
					nl2br(htmlspecialchars( $descrip )),
					nl2br(htmlspecialchars( $ingred )),
					nl2br(htmlspecialchars( $steps ))
					
				);
				if( $video ){
					printf("<iframe width='420' height='315' src='//www.youtube.com/embed/%s' frameborder='0' allowfullscreen></iframe>",
					       htmlspecialchars( $video ));
				}
				printf("
			<form action='view_private.php'  method='GET'>
				Scale this Recipe!
				<label>Factor: <input type='number' step = 'any' name='scale'/></label>
				<input type='hidden' name='p' value=%s >
				<input type='submit' value='Scale this Recipe!' class= 'button' />
			", htmlspecialchars($id));
			print("</div>");
			}		
			$story_data->close();
		?>
	</article>
	</div>
	</div>
	
	<script src="js/jquery.js"></script>
	<script src="js/foundation.min.js"></script>
	<script>
	$(document).foundation();
	</script>
	
	</body>
</html>
