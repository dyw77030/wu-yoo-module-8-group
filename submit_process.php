<?php
session_start();

//Checking to see if the user is signed in and has submitted information for all fields.
if(!isset($_SESSION['openid.identifier'])){
	header("Location: login.html");
	exit;
}

require 'database.php';
//Checking CSRF token
if($_SESSION['token'] != $_POST['token']){
	die("Request forgery detected");
}

//Variable santization
$video_regex = "/youtube.com[\S]*([\w_-]{11}$)/";
$video_match = "";
$image_regex = "/.jpg$|.png$|.gif$/";

$title = $_POST['title'];
$description = $_POST['description'];
$ingredients = $_POST['ingredients'];
$steps = $_POST['steps'];
$poster_id = $_SESSION['openid.identifier'];
$category = $_POST['category'];
if(isset($_POST['video'])){
	if(preg_match($video_regex, $_POST['video'], $video_match)){
		$video = $video_match[1];
	}else{
		$video = null;
	}
}else $video = null;

//Checking for duplicate title
$stmt2 = $mysqli->prepare("SELECT count(*) FROM public WHERE title = ?");
if(!$stmt2){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}

$stmt2->bind_param('s', $title);
$stmt2->execute();

$stmt2->bind_result($cnt);
$stmt2->fetch();
$stmt2->close();

//Insertion
if ($cnt == 0){
	$stmt = $mysqli->prepare("INSERT INTO public (title, ingred, steps, user, video, descrip, category) VALUES (?, ?, ?, ?, ?, ?, ?)");
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	$stmt -> bind_param('sssssss', $title, $ingredients, $steps, $poster_id, $video, $description, $category);
	$stmt -> execute();
	$stmt -> close();

	$stmt4 = $mysqli->prepare("SELECT id from public where title = ?");
	if(!$stmt4){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	$stmt4 -> bind_param('s', $title);
	$stmt4 -> execute();
	$stmt4 -> bind_result($id);
	$stmt4 -> fetch();
	$stmt4 -> close();
	
	$filename = basename($_FILES['image']['name']);
	if(!empty($filename)){
		if(preg_match($image_regex, $filename)){
			$full_path = sprintf("images/%s",$filename);
			if( move_uploaded_file($_FILES['image']['tmp_name'], $full_path) ){
			}
			$imagesql = $mysqli->prepare("UPDATE public SET img = ? WHERE id = ?");
			if(!$imagesql){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}
			$data_path = sprintf("/~dyw77030/Module8-w/%s", $full_path);
			$imagesql -> bind_param('ss', $data_path, $id);
			$imagesql -> execute();
			$imagesql -> close();
		}else{
			$image = null;
		}
	}
	header("Location: index.php");
	exit;
}
else{
header("Location: index.php");
	exit;
	}
?>