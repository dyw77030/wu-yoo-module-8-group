<?php
session_start();
//Checking to see if the user is signed in and has submitted information for all fields.
if(!isset($_SESSION['openid.identifier'])){
	header("Location: login.html");
	exit;
}


//Checking CSRF token
if($_SESSION['token'] !== $_POST['token']){
	die("Request forgery detected");
}

require 'database.php';

$title = $_POST['title'];
$description = $_POST['description'];
$ingredients = $_POST['ingredients'];
$steps = $_POST['steps'];
$user = $_SESSION['openid.identifier'];
$category = $_POST['category'];
$id = $_POST['id'];


//Insertion
	$stmt = $mysqli->prepare("UPDATE public SET title = ? , descrip = ? , ingred = ? , steps = ? , category = ? WHERE id = ? and user = ?");
	if(!$stmt){
		printf("Query Prep Failed3: %s\n", $mysqli->error);
		exit;
	}
	$stmt -> bind_param('sssssis', $title, $description, $ingredients, $steps, $category,  $id, $user);
	$stmt -> execute();
	$stmt -> close();
	header("Location: index.php");
	exit;
?>